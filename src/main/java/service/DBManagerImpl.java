package service;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.text.SimpleDateFormat;

public class DBManagerImpl implements DBManager {

	private Connection connect;
    private Statement stmt;
    private PreparedStatement pstmt;
    private ResultSet resultSet;
    private static final String START_INSERT = "insert into iothermostat.thermo_log "
    		+ "values (default, ?, null, ?, null, ?, null)";
    private static final String UPDATE_ROW = "update iothermostat.thermo_log set time_end = ?"
    		+ ", temp_end = ?, hum_end = ? order by id desc limit 1";
    //private static final String GET_ALL_ROWS = "select * from iothermostat";

    public DBManagerImpl() throws Exception {
    	try {
    		Class.forName("com.mysql.cj.jdbc.Driver");
            /* Setup the connection with the DB */
    		connect = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/iothermostat", "root", "");
    	} catch (Exception e) {
    		throw e;
    	}
    }

	/**
	 * Create a new record in the database
	 * @throws Exception
	 */
	@Override
	public void insertThermoOn(final float temp, final float humidity) throws Exception {
		try {
			pstmt = connect.prepareStatement(START_INSERT);
			pstmt.setFloat(1, temp);
			pstmt.setString(2, this.dateFormatter());
			pstmt.setFloat(3, humidity);
			pstmt.executeUpdate();
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void insertThermoOff(final float temp, final float humidity) throws Exception {
		try {
			pstmt = connect.prepareStatement(UPDATE_ROW);
			pstmt.setString(1, this.dateFormatter());  //1 means the first "?"
			pstmt.setFloat(2, temp);
			pstmt.setFloat(3, humidity);
			//System.out.println(this.dateFormatter());
			pstmt.executeUpdate();
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Utility method, here you can get some metadata from the database
	 * @param resultSet get the result of the SQL query
	 * @throws SQLException
	 */
    private void writeMetaData(ResultSet resultSet) throws SQLException {
        System.out.println("The columns in the table are: ");
        System.out.println("Table: " + resultSet.getMetaData().getTableName(1));
        for  (int i = 1; i<= resultSet.getMetaData().getColumnCount(); i++){
            System.out.println("Column " +i  + " "+ resultSet.getMetaData().getColumnName(i));
        }
    }

    /**Utility method
     * @return a DateTime without nanoSec, compatible with MySQL type DateTime.
     */
    private String dateFormatter() {
    	Date dt = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String currentTime = sdf.format(dt);
			return currentTime;
    }

    /**
     * Utility method to close the connection.
     */
    private void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (stmt != null) {
                stmt.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }

}
