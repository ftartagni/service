package service;

public interface DBManager {

	/**
	 * Creates a new record in the database
	 * @throws Exception
	 */
	void insertThermoOn(final float temp, final float humidity) throws Exception;

	/**
	 * Inserts temp_end and time_end and hum_end in the database
	 * @throws Exception
	 */
	void insertThermoOff(final float temp, final float humidity) throws Exception;

}