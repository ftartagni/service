package service;

class DataPoint {
	private float value;
	private long time;
	private float hum;
	
	public DataPoint(float value, long time, float hum) {
		this.value = value;
		this.time = time;
		this.hum = hum;
	}
	
	public float getValue() {
		return value;
	}
	
	public long getTime() {
		return time;
	}
	
	public float getHumidity() {
		return hum;
	}

	@Override
	public String toString() {
		return "DataPoint [value=" + value + ", time = " + time + ", hum = " + hum + "]";
	}
}
