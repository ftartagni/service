package service;

import java.util.LinkedList;

public class SerialMsg extends Thread {
	private static final String OFF = "OFF";
	private static final String ON = "ON";
	private final int step;
	private float value;
	private float hum;
	private final DataService data;
	private final SerialCommChannel channel = new SerialCommChannel("COM5",9600);
	private final DBManager dbm;

	public SerialMsg(final int step, final DataService data, final DBManager dbManager) throws Exception {
		this.step = step;
		this.data = data;
		this.dbm = dbManager;
	}

	public void run() {
		while(true) {
			var list = new LinkedList<>(data.getData());
			DataPoint data = list.getFirst();
			this.value = data.getValue();
			this.hum = data.getHumidity();
			channel.sendMsg(Float.toString(this.value));
			try {
				Thread.sleep(this.step);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			if (channel.isMsgAvailable()) {
				try {
					this.DBOperation(this.value, this.hum);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 *  Put msg.equals() and not msg.contains() if not debugging
	 * @param tempValue
	 * @param humidity
	 * @throws Exception
	 */
	private void DBOperation(final float tempValue, final float humidity) throws Exception {
			String msg = channel.receiveMsg();
			if (msg.contains(OFF)) {
				this.dbm.insertThermoOff(tempValue, humidity);
				System.out.println("[THERMO OFF]");
			} else if (msg.contains(ON)) {
				this.dbm.insertThermoOn(tempValue, humidity);
				System.out.println("[THERMO ON]");
			} else {    /* Use only for Debugging */
				System.out.println("Received:" + msg);
			}
	}
}
