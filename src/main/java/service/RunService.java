package service;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;

/*
 * Data Service as a vertx event-loop 
 */
public class RunService extends AbstractVerticle {


	public static void main(String[] args) throws Exception {
		Vertx vertx = Vertx.vertx();
		DataService service = new DataService(8080);
		vertx.deployVerticle(service);
		/* Waiting to setup Edge and Controller correctly. */
		Thread.sleep(7000);
		SerialMsg serialMsg = new SerialMsg(2000, service, new DBManagerImpl());
		serialMsg.start();
	}
}